import 'package:flutter/material.dart';

class DropDownWidget extends StatefulWidget {
  DropDownWidget({Key? key}) : super(key: key);
  @override
  DropDownWidgetState createState() => DropDownWidgetState();
}

class DropDownWidgetState extends State<DropDownWidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          appBar: AppBar(title: Text('DropDown')),
          body: Column(
            children: [
              Container(
                child: Row(children: [
                  Text('Vaccine:'),
                  Expanded(child: Container(),),
                  DropdownButton(items: [
                    DropdownMenuItem(child: Text('-'),value: '-'),
                    DropdownMenuItem(child: Text('Pfizer'),value: 'Pfizer'),
                    DropdownMenuItem(child: Text('J&J'),value: 'J&J'),
                    DropdownMenuItem(child: Text('Sputnik'),value: 'Sputnik'),
                    DropdownMenuItem(child: Text('AstraZeneca'),value: 'AstraZeneca'),
                    DropdownMenuItem(child: Text('Novavax'),value: 'Novavax'),
                    DropdownMenuItem(child: Text('Sinopharm'),value: 'Sinopharm'),
                    DropdownMenuItem(child: Text('Sinovac'),value: 'Sinovac'),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                  )
                ],),
              ),
              Center(
                child: Text(vaccine,
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
              )
            ],
          )),
    );
  }
}
