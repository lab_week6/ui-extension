
import 'package:flutter/material.dart';

Widget checkbox(String title, bool value,ValueChanged<bool?>? onChanged){
  return Container(
            padding: EdgeInsets.only(left: 16.0, right: 16.0, bottom: 8.0),
            child: Column(
              children: [
              Row(
              children: [
              Text(title),
              Expanded(
                child: Container(),),
              Checkbox(
                value: value, 
              onChanged: onChanged
              )
              ],
            ),
            Divider()
            ]
            ));


}

class CheckBoxWidget extends StatefulWidget {
  CheckBoxWidget({Key? key}) : super(key: key);
  @override
  CheckBoxWidgetState createState() => CheckBoxWidgetState();
}

class CheckBoxWidgetState extends State<CheckBoxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Checkbox')),
        body: ListView(
          children: [
         checkbox('check 1',check1,(value){
           setState(() {
             check1 = value!;
           });
         }),
          checkbox('check 2',check2,(value){
           setState(() {
             check2 = value!;
           });
         }),
          checkbox('check 3',check3,(value){
           setState(() {
             check3 = value!;
           });
         }),
         TextButton(child: Text('Save'),
         onPressed: (){
           ScaffoldMessenger.of(context).showSnackBar(SnackBar(
             content: Text(
               'check1: $check1, check2: $check2, check3: $check3'
             )
           ));
         },)
          ],
          )
    );
  }
}
